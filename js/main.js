'use strict';

function Player() {
  this.init = function() {
    this.$audio = $('#player-audio');
    this.audio = this.$audio.get()[0];
    this.audio.addEventListener('playing', function(event) { $('#player-spinner-overlay').hide(); });
    // this.audio.addEventListener('stalled', function(event) { $('#player-spinner-overlay').show(); });
    this.audio.addEventListener('waiting', function(event) { $('#player-spinner-overlay').show(); });
  }

  this.play = function() {
    $('#player-playstop-overlay').hide();
    var nocache = Date.now();
    this.$audio.empty();
    this.$audio.append('<source src="https://live.helsinki.at:8088/live128.ogg?nocache=' + nocache + '" type="audio/ogg" />');
    this.$audio.append('<source src="https://live.helsinki.at:8088/live128.mp3?nocache=' + nocache + '" type="audio/mpeg" />');
    this.audio.load();
    this.audio.play();
  }

  this.stop = function() {
    $('#player-playstop-overlay').show();
    $('#player-spinner-overlay').hide();
    this.audio.pause();
    this.$audio.empty();
    this.audio.load();
  }

  this.playstop = function() {
    if(this.audio.paused == true) {
      this.play();
    } else {
      this.stop();
    }
  }

  this.repaintVolumeControls = function() {
    if(this.audio.muted) {
      $('#player-mute').css('background-position', '0px -150px');
      return;
    }

    if(this.audio.volume <= 0) {
      $('#player-mute').css('background-position', '-25px -150px');
      return;
    }

    if(this.audio.volume < 0.5) {
      $('#player-mute').css('background-position', '-50px -150px');
      return;
    }

    $('#player-mute').css('background-position', '-75px -150px');
  }

  this.updatevolume = function() {
    this.audio.volume = $('#player-volume').val() / 100;
    this.repaintVolumeControls();
  }

  this.togglemute = function() {
    this.audio.muted = !this.audio.muted;
    $('#player-volume').prop('disabled', this.audio.muted);
    this.repaintVolumeControls();
  }
}

var player = new Player();

function player_init() {
  player.init();
  $('#player-playstop').on('click', player_playstop);
  if(navigator.userAgent.match(/(\(iPod|\(iPhone|\(iPad)/)) {
    $('#player-volume').prop('disabled', true);
    this.repaintVolumeControls();
  } else {
    $('#player-volume').on('change input', player_updatevolume);
    $('#player-mute').on('click', player_togglemute);
  }

  $(document).on('keypress', function(e) {
    if(e.which == 32) {
      player.playstop();
    }
  });
}

function player_playstop() {
  player.playstop();
}

function player_updatevolume() {
  player.updatevolume();
}

function player_togglemute() {
  player.togglemute();
}
